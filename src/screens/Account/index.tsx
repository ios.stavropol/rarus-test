import React, {useCallback, useEffect, useRef} from 'react';
import {View, Image, Text, TouchableOpacity, FlatList} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation} from '@react-navigation/native';

const rows = [
  {id: 1, title: 'Favorite', route: 'Favorite'},
  {id: 2, title: 'Edit Account', route: ''},
  {id: 3, title: 'Settings and Privacy', route: ''},
  {id: 4, title: 'Help', route: ''},
];

const AccountScreen = ({}) => {
  const navigation = useNavigation();

  const [login, setLogin] = React.useState('');

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={{
          width: Common.getLengthByIPhone7(0),
          height: Common.getLengthByIPhone7(48),
          marginBottom: Common.getLengthByIPhone7(12),
          marginTop: index === 0 ? Common.getLengthByIPhone7(24) : 0,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingLeft: Common.getLengthByIPhone7(20),
          paddingRight: Common.getLengthByIPhone7(16),
        }}
        onPress={() => {
          navigation.navigate(item.route);
        }}>
        <Text style={[typography.CAPTION, {}]} allowFontScaling={false}>
          {item.title}
        </Text>
        <Image
          source={require('./../../assets/ic-arrow-right.png')}
          style={{
            width: Common.getLengthByIPhone7(16),
            height: Common.getLengthByIPhone7(16),
          }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <Image
        source={require('./../../assets/ic-avatar2.png')}
        style={{
          width: Common.getLengthByIPhone7(64),
          height: Common.getLengthByIPhone7(64),
          marginTop: Common.getLengthByIPhone7(24),
        }}
      />
      <FlatList
        style={{
          marginTop: Common.getLengthByIPhone7(24),
        }}
        data={rows}
        keyExtractor={(item, index) => item.id}
        renderItem={renderItem}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(AccountScreen);
