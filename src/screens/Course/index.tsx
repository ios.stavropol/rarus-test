import React, {useCallback, useEffect, useRef} from 'react';
import {View, Text, Image} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation} from '@react-navigation/native';
import SearchView from '../../components/SearchView';
import TabView from '../../components/Course/TabView';
import CourcesTable from '../../components/Course/CourcesTable';

const CourseScreen = ({getCources, setCources, courcesRaw}) => {
  const navigation = useNavigation();

  useEffect(() => {
    getCources();
  }, []);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <SearchView
        style={{
          marginTop: Common.getLengthByIPhone7(16),
        }}
        onSearch={text => {
          console.warn(text);
          let array = courcesRaw;
          if (text?.length) {
            array = array.filter(obj => {
              return (
                (obj.name + obj.speaker)
                  .toLocaleLowerCase()
                  .indexOf(text.toLocaleLowerCase()) !== -1
              );
            });
          }
          setCources(array);
        }}
      />
      <View
        style={{
          marginTop: Common.getLengthByIPhone7(4),
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <Image
          source={require('./../../assets/ic-img1.png')}
          style={{
            width: Common.getLengthByIPhone7(160),
            height: Common.getLengthByIPhone7(108),
            resizeMode: 'contain',
          }}
        />
        <Image
          source={require('./../../assets/ic-img2.png')}
          style={{
            width: Common.getLengthByIPhone7(160),
            height: Common.getLengthByIPhone7(108),
            resizeMode: 'contain',
          }}
        />
      </View>
      <TabView
        style={{
          marginTop: Common.getLengthByIPhone7(34),
        }}
        onChange={res => {}}
      />
      <CourcesTable
        style={{
          marginTop: Common.getLengthByIPhone7(13),
          marginBottom: 111,
        }}
        onRefresh={() => {
          getCources();
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({
  cources: state.user.cources,
  courcesRaw: state.user.courcesRaw,
});

const mdtp = (dispatch: Dispatch) => ({
  getCources: () => dispatch.user.getCources(),
  setCources: payload => dispatch.user.setCources(payload),
});

export default connect(mstp, mdtp)(CourseScreen);
