import React, {useCallback, useEffect, useRef} from 'react';
import {View, Text} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation} from '@react-navigation/native';

const SearchScreen = ({}) => {
  const navigation = useNavigation();

  const [login, setLogin] = React.useState('');
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
      }}></View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(SearchScreen);
