import React, {useCallback, useEffect, useRef} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation} from '@react-navigation/native';
import TodayView from '../../components/Home/TodayView';
import HorizontalView from '../../components/Home/HorizontalView';
import AdsView from '../../components/Home/AdsView';
import PlanView from '../../components/Home/PlanView';

const HomeScreen = ({}) => {
  const navigation = useNavigation();

  const [login, setLogin] = React.useState('');
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BLUE_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <View
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
          position: 'absolute',
          left: 0,
          top: Common.getLengthByIPhone7(90),
          bottom: 0,
          backgroundColor: 'white',
        }}
      />
      <Text
        style={[
          typography.SUBTITLE,
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            color: 'white',
          },
        ]}
        allowFontScaling={false}>
        Let’s start learning
      </Text>
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
          marginBottom: 111,
          marginTop: 20,
        }}
        contentContainerStyle={{
          alignItems: 'center',
        }}>
        <TodayView style={{}} value={Math.floor(Math.random() * 60)} max={60} />
        <HorizontalView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
        />
        <PlanView
          style={{
            marginTop: Common.getLengthByIPhone7(20),
          }}
        />
        <AdsView
          style={{
            marginTop: Common.getLengthByIPhone7(20),
          }}
        />
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(HomeScreen);
