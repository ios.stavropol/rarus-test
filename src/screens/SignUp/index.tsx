import React, {useCallback, useEffect, useRef} from 'react';
import {View, Text} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation} from '@react-navigation/native';
import TextView from '../../components/SignUp/TextView';
import TickView from '../../components/SignUp/TickView';
import Button from '../../components/SignUp/Button';
import ModalView from '../../components/SignUp/ModalView';

const SignUpScreen = ({}) => {
  const navigation = useNavigation();

  const [login, setLogin] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [loginError, setLoginError] = React.useState(false);
  const [passwordError, setPasswordError] = React.useState(false);
  const [disabled, setDisabled] = React.useState(true);
  const [show, setShow] = React.useState(false);

  const next = () => {
    if (login?.length && password?.length) {
      setLoginError(false);
      setPasswordError(false);
      setShow(true);
    } else {
      if (!login?.length) {
        setLoginError(true);
      }
      if (!password?.length) {
        setPasswordError(true);
      }
    }
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BLUE_COLOR,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
      }}>
      <Text
        style={[
          typography.TITLE_32,
          {
            marginLeft: Common.getLengthByIPhone7(24),
            marginTop: Common.getLengthByIPhone7(80),
            color: 'white',
          },
        ]}
        allowFontScaling={false}>
        Sign Up
      </Text>
      <Text
        style={[
          typography.SUBTITLE,
          {
            marginLeft: Common.getLengthByIPhone7(24),
            marginTop: Common.getLengthByIPhone7(4),
            color: colors.LIGHT_BLUE_COLOR,
            fontSize: Common.getLengthByIPhone7(12),
          },
        ]}
        allowFontScaling={false}>
        Enter your details below & free sign up
      </Text>
      <View
        style={{
          width: Common.getLengthByIPhone7(0),
          borderRadius: Common.getLengthByIPhone7(16),
          flex: 1,
          backgroundColor: 'white',
          marginTop: Common.getLengthByIPhone7(20),
          alignItems: 'center',
        }}>
        <TextView
          title={'Your  Email'}
          placeholder={'Required'}
          autoFocus
          secureTextEntry={false}
          error={loginError}
          style={{marginTop: Common.getLengthByIPhone7(32)}}
          onChange={text => {
            setLogin(text);
          }}
        />
        <TextView
          title={'Password'}
          placeholder={'Required'}
          secureTextEntry
          error={passwordError}
          style={{marginTop: Common.getLengthByIPhone7(24)}}
          onChange={text => {
            setPassword(text);
          }}
        />
        <TickView
          style={{
            marginTop: Common.getLengthByIPhone7(29),
          }}
          onChange={res => {
            setDisabled(!res);
          }}
        />
        <Button
          style={{
            marginTop: Common.getLengthByIPhone7(26),
          }}
          disabled={disabled}
          onClick={() => {
            next();
          }}
        />
      </View>
      <ModalView
        show={show}
        onClose={() => {
          setShow(false);
          navigation.navigate('LoginIn');
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(SignUpScreen);
