import React, {useCallback, useEffect, useRef} from 'react';
import {
  Dimensions,
  View,
  Animated,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation} from '@react-navigation/native';

const pages = [1, 2, 3];
const {width} = Dimensions.get('window');

const IntroScreen = ({}) => {
  const navigation = useNavigation();

  scrollX = new Animated.Value(0);
  const [page, setPage] = React.useState(0);
  const [signup, setSignup] = React.useState(null);
  const [skip, setSkip] = React.useState(null);

  useEffect(() => {
    if (page === 2) {
      setSignup(
        <TouchableOpacity
          style={{
            width: Common.getLengthByIPhone7(160),
            height: Common.getLengthByIPhone7(50),
            borderRadius: Common.getLengthByIPhone7(12),
            backgroundColor: colors.BLUE_COLOR,
            alignItems: 'center',
            justifyContent: 'center',
            position: 'absolute',
            bottom: Common.getLengthByIPhone7(81),
          }}
          onPress={() => {
            navigation.navigate('SignUp');
          }}>
          <Text
            style={[
              typography.CAPTION,
              {
                textAlign: 'center',
                color: 'white',
              },
            ]}
            allowFontScaling={false}>
            Sign up
          </Text>
        </TouchableOpacity>,
      );
      setSkip(null);
    } else {
      setSkip(
        <TouchableOpacity
          style={{
            width: Common.getLengthByIPhone7(45),
            height: Common.getLengthByIPhone7(40),
            alignItems: 'center',
            justifyContent: 'center',
            position: 'absolute',
            top: Common.getLengthByIPhone7(60),
            right: Common.getLengthByIPhone7(16),
          }}
          onPress={() => {
            navigation.navigate('SignUp');
          }}>
          <Text
            style={[
              typography.CAPTION,
              {
                textAlign: 'center',
                color: colors.GRAY_COLOR,
              },
            ]}
            allowFontScaling={false}>
            Skip
          </Text>
        </TouchableOpacity>,
      );
      setSignup(null);
    }
  }, [page]);

  const renderPage = (title, subtitle, image) => {
    return (
      <View
        style={{
          width: Common.getLengthByIPhone7(0),
          alignItems: 'center',
          marginTop: Common.getLengthByIPhone7(112),
        }}>
        <Image
          source={image}
          style={{
            width: Common.getLengthByIPhone7(260),
            height: Common.getLengthByIPhone7(260),
            resizeMode: 'contain',
          }}
        />
        <Text
          style={[
            typography.TITLE,
            {
              textAlign: 'center',
              marginTop: Common.getLengthByIPhone7(34),
              width: Common.getLengthByIPhone7(220),
            },
          ]}
          allowFontScaling={false}>
          {title}
        </Text>
        <Text
          style={[
            typography.SUBTITLE,
            {
              color: colors.GRAY_COLOR,
              textAlign: 'center',
              marginTop: Common.getLengthByIPhone7(20),
              width: Common.getLengthByIPhone7(220),
            },
          ]}
          allowFontScaling={false}>
          {subtitle}
        </Text>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          height: Dimensions.get('screen').height,
        }}
        contentContainerStyle={{
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}
        horizontal={true}
        pagingEnabled={true}
        showsHorizontalScrollIndicator={false}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {
            listener: event => {
              let page = Math.round(
                parseFloat(
                  event.nativeEvent.contentOffset.x /
                    Dimensions.get('window').width,
                ),
              );
              setPage(page);
            },
          },
        )}
        scrollEventThrottle={16}
        bounces={false}>
        {renderPage(
          'Numerous free trial courses',
          'Free courses for you to find your way to learning',
          require('./../../assets/ic-intro1.png'),
        )}
        {renderPage(
          'Quick and easy learning',
          'Easy and fast learning at any time to help you improve various skills',
          require('./../../assets/ic-intro2.png'),
        )}
        {renderPage(
          'Create your own study plan',
          'Study according to the study plan, make study more motivated',
          require('./../../assets/ic-intro3.png'),
        )}
      </ScrollView>
      <View
        style={{
          flexDirection: 'row',
          position: 'absolute',
          bottom: Common.getLengthByIPhone7(200),
        }}>
        {pages.map((_, i) => {
          let backgroundColor = Animated.divide(scrollX, width).interpolate({
            inputRange: [i - 1, i, i + 1],
            outputRange: ['#EAEAFF', colors.BLUE_COLOR, '#EAEAFF'],
            extrapolate: 'clamp',
          });

          return (
            <Animated.View
              key={i}
              style={{
                backgroundColor,
                height: Common.getLengthByIPhone7(5),
                width: Animated.divide(scrollX, width).interpolate({
                  inputRange: [i - 1, i, i + 1],
                  outputRange: [
                    Common.getLengthByIPhone7(9),
                    Common.getLengthByIPhone7(28),
                    Common.getLengthByIPhone7(9),
                  ],
                  extrapolate: 'clamp',
                }),
                marginLeft: Common.getLengthByIPhone7(5),
                marginRight: Common.getLengthByIPhone7(5),
                borderRadius: Common.getLengthByIPhone7(5),
              }}
            />
          );
        })}
      </View>
      {signup}
      {skip}
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(IntroScreen);
