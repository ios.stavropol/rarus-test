import React, {useEffect, useRef} from 'react';
import {Image, View, Text} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors, typography, shadows} from '../../styles';

const AdsView = ({style, value, max}) => {
  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          borderRadius: Common.getLengthByIPhone7(12),
          padding: Common.getLengthByIPhone7(16),
          backgroundColor: '#EFE0FF',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        },
        style,
      ]}>
      <View style={{}}>
        <Text
          style={[
            typography.CAPTION,
            {
              fontSize: Common.getLengthByIPhone7(24),
              color: colors.VIOLET_COLOR,
            },
          ]}
          allowFontScaling={false}>
          Meetup
        </Text>
        <Text
          style={[
            typography.SUBTITLE,
            {
              fontSize: Common.getLengthByIPhone7(12),
              color: colors.VIOLET_COLOR,
              width: Common.getLengthByIPhone7(190),
              lineHeight: null,
            },
          ]}
          allowFontScaling={false}>
          Off-line exchange of learning experiences
        </Text>
      </View>
      <Image
        source={require('./../../assets/ic-ads.png')}
        style={{
          width: Common.getLengthByIPhone7(96),
          height: Common.getLengthByIPhone7(96),
          resizeMode: 'contain',
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(AdsView);
