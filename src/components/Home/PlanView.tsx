import React, {useEffect, useRef} from 'react';
import {Image, TextInput, View, Text, TouchableOpacity} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors, typography, shadows} from '../../styles';
// import {AnimatedCircularProgress} from 'react-native-circular-progress';
import AnimatedCircularProgress from 'react-native-conical-gradient-progress';

const PlanView = ({style}) => {
  const renderRow = (title, value, max) => {
    return (
      <View
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          paddingLeft: Common.getLengthByIPhone7(16),
          paddingRight: Common.getLengthByIPhone7(16),
          marginTop: Common.getLengthByIPhone7(14),
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={{
              transform: [{rotateY: '180deg'}],
            }}>
            <AnimatedCircularProgress
              size={Common.getLengthByIPhone7(18)}
              width={3}
              fill={(value * 100) / max}
              prefill={0}
              beginColor={'#FF4D00'}
              endColor={colors.TABBAR_INACTIVE_COLOR}
              segments={100}
              backgroundColor={colors.TABBAR_INACTIVE_COLOR}
              linecap="round"
            />
          </View>
          <Text
            style={[
              typography.TITLE,
              {
                fontSize: Common.getLengthByIPhone7(14),
                marginLeft: Common.getLengthByIPhone7(10),
              },
            ]}
            allowFontScaling={false}>
            {title}
          </Text>
        </View>
        <Text
          style={[
            typography.SUBTITLE,
            {
              fontSize: Common.getLengthByIPhone7(14),
            },
          ]}
          allowFontScaling={false}>
          {value}/
          <Text
            style={[
              typography.SUBTITLE,
              {
                fontSize: Common.getLengthByIPhone7(14),
                color: colors.GRAY_COLOR,
              },
            ]}
            allowFontScaling={false}>
            {max}
          </Text>
        </Text>
      </View>
    );
  };

  return (
    <View
      style={[
        {
          shadowColor: 'rgba(184, 184, 210)',
          shadowOffset: {
            width: 0,
            height: 4,
          },
          shadowOpacity: 0.02,
          shadowRadius: -10,
          borderRadius: Common.getLengthByIPhone7(12),
          backgroundColor: 'white',
        },
        style,
      ]}>
      <View
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          },
        ]}>
        <Text
          style={[
            typography.TITLE,
            {
              fontSize: Common.getLengthByIPhone7(18),
            },
          ]}
          allowFontScaling={false}>
          Learning Plan
        </Text>
        {renderRow('Packaging Design', Math.floor(Math.random() * 48), 48)}
        {renderRow('Product Design', Math.floor(Math.random() * 24), 24)}
      </View>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(PlanView);
