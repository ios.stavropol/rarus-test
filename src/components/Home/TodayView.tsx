import React, {useEffect, useRef} from 'react';
import {Image, TextInput, View, Text, TouchableOpacity} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors, typography} from '../../styles';
import LinearGradient from 'react-native-linear-gradient';

const TodayView = ({style, value, max}) => {
  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          borderRadius: Common.getLengthByIPhone7(12),
          padding: Common.getLengthByIPhone7(16),
          backgroundColor: 'white',
        },
        style,
      ]}>
      <View
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(72),
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <Text
          style={[
            typography.SUBTITLE,
            {
              fontSize: Common.getLengthByIPhone7(12),
              color: colors.GRAY_COLOR,
            },
          ]}
          allowFontScaling={false}>
          Learned today
        </Text>
        <Text
          style={[
            typography.SUBTITLE,
            {
              fontSize: Common.getLengthByIPhone7(12),
              color: colors.BLUE_COLOR,
            },
          ]}
          allowFontScaling={false}>
          My courses
        </Text>
      </View>
      <Text
        style={[
          typography.TITLE,
          {
            fontSize: Common.getLengthByIPhone7(20),
          },
        ]}
        allowFontScaling={false}>
        {value}min{' '}
        <Text
          style={[
            typography.SUBTITLE,
            {
              fontSize: Common.getLengthByIPhone7(10),
              color: colors.GRAY_COLOR,
            },
          ]}
          allowFontScaling={false}>
          / {max}min
        </Text>
      </Text>
      <View
        style={{
          marginTop: Common.getLengthByIPhone7(10),
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(72),
          height: Common.getLengthByIPhone7(6),
          borderRadius: Common.getLengthByIPhone7(22),
          backgroundColor: colors.TABBAR_INACTIVE_COLOR,
          overflow: 'hidden',
        }}>
        <LinearGradient
          colors={['transparent', '#FF5106']}
          start={{x: 0.0, y: 0.5}}
          end={{x: 1.0, y: 0.5}}
          style={{
            width:
              ((Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(72)) *
                value) /
              max,
            height: Common.getLengthByIPhone7(6),
            position: 'absolute',
            left: 0,
            top: 0,
          }}
        />
      </View>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(TodayView);
