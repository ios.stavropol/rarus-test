import React, {useEffect, useRef} from 'react';
import {Image, ScrollView, View, Text, TouchableOpacity} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors, typography} from '../../styles';
import LinearGradient from 'react-native-linear-gradient';

const HorizontalView = ({style, value, max}) => {
  const renderView = (icon, style) => {
    return (
      <View
        style={[
          {
            width: Common.getLengthByIPhone7(250),
            height: Common.getLengthByIPhone7(155),
            borderRadius: Common.getLengthByIPhone7(16),
            overflow: 'hidden',
            backgroundColor: 'white',
          },
          style,
        ]}>
        <Image
          source={icon}
          style={{
            width: Common.getLengthByIPhone7(250),
            height: Common.getLengthByIPhone7(155),
          }}
        />
      </View>
    );
  };

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0),
          height: Common.getLengthByIPhone7(155),
        },
        style,
      ]}>
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          height: Common.getLengthByIPhone7(155),
        }}
        horizontal={true}
        pagingEnabled={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {renderView(require('./../../assets/illustration1.png'), {
          marginLeft: Common.getLengthByIPhone7(20),
        })}
        {renderView(require('./../../assets/illustration2.png'), {
          marginLeft: Common.getLengthByIPhone7(12),
          marginRight: Common.getLengthByIPhone7(20),
        })}
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(HorizontalView);
