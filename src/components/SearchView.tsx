import React, {useCallback, useEffect, useRef} from 'react';
import {
  Image,
  TextInput,
  View,
  TouchableOpacity,
  Linking,
  Text,
  Alert,
} from 'react-native';
import {RootState, Dispatch} from './../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Common from '../utilities/Common';
import {colors, typography} from './../styles';

const SearchView = ({onSearch, value, onSubmit, style}) => {
  const [text, setText] = React.useState('');

  useEffect(() => {
    setText(value);
  }, [value]);

  useEffect(() => {
    if (onSearch) {
      onSearch(text);
    }
  }, [text]);

  return (
    <View
      style={[
        {
          alignItems: 'center',
          justifyContent: 'center',
        },
        style,
      ]}>
      <TextInput
        style={[
          typography.CAPTION,
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            height: Common.getLengthByIPhone7(48),
            borderRadius: Common.getLengthByIPhone7(12),
            paddingLeft: Common.getLengthByIPhone7(44),
            paddingRight: Common.getLengthByIPhone7(15),
            backgroundColor: colors.TABBAR_INACTIVE_COLOR,
          },
        ]}
        placeholderTextColor={colors.BORDER_COLOR}
        placeholder={'Find course'}
        returnKeyType={'done'}
        allowFontScaling={false}
        underlineColorAndroid={'transparent'}
        onSubmitEditing={() => {
          if (onSubmit) {
            onSubmit(text);
          }
        }}
        onFocus={() => {}}
        onBlur={() => {}}
        onChangeText={code => {
          setText(code);
        }}
        value={text}
      />
      <Image
        source={require('./../assets/ic-tab3.png')}
        style={{
          width: Common.getLengthByIPhone7(16),
          height: Common.getLengthByIPhone7(16),
          resizeMode: 'contain',
          position: 'absolute',
          left: Common.getLengthByIPhone7(16),
          tintColor: colors.BORDER_COLOR,
        }}
      />
    </View>
  );
};

export default SearchView;
