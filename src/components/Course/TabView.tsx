import React, {useEffect, useRef} from 'react';
import {Image, TextInput, View, Text, TouchableOpacity} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors, typography} from '../../styles';

const TabView = ({style, onChange}) => {
  const [selected, setSelected] = React.useState(0);

  useEffect(() => {
    if (onChange) {
      onChange(selected);
    }
  }, [selected]);

  const renderButton = (index, title, onPress) => {
    return (
      <TouchableOpacity
        style={{
          paddingLeft: Common.getLengthByIPhone7(28),
          paddingRight: Common.getLengthByIPhone7(28),
          height: Common.getLengthByIPhone7(28),
          borderRadius: Common.getLengthByIPhone7(20),
          backgroundColor:
            selected === index ? colors.BLUE_COLOR : 'transparent',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        key={index}
        activeOpacity={1}
        onPress={() => {
          setSelected(index);
        }}>
        <Text
          style={[
            typography.SUBTITLE,
            {
              color: selected === index ? 'white' : colors.LIGHT_BLUE_COLOR,
            },
          ]}
          allowFontScaling={false}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        },
        style,
      ]}>
      <Text
        style={[
          typography.CAPTION,
          {
            fontSize: Common.getLengthByIPhone7(18),
          },
        ]}
        allowFontScaling={false}>
        Choice your course
      </Text>
      <View
        style={{
          marginTop: Common.getLengthByIPhone7(22),
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        {renderButton(0, 'All', () => {})}
        {renderButton(1, 'Popular', () => {})}
        {renderButton(2, 'New', () => {})}
      </View>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(TabView);
