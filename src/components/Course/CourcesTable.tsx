import React, {useEffect, useRef} from 'react';
import {
  Image,
  FlatList,
  View,
  Text,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors, typography} from '../../styles';

const images = [
  require('./../../assets/courseImage1.png'),
  require('./../../assets/courseImage2.png'),
  require('./../../assets/courseImage3.png'),
  require('./../../assets/courseImage4.png'),
  require('./../../assets/courseImage5.png'),
  require('./../../assets/courseImage6.png'),
];

const CourcesTable = ({style, cources, onRefresh, isRequestGoing}) => {
  const [body, setBody] = React.useState();

  useEffect(() => {
    setBody(renderList());
  }, []);

  useEffect(() => {
    if (isRequestGoing) {
      setBody(renderIndicator());
    } else {
      setBody(renderList());
    }
  }, [isRequestGoing]);

  const renderIndicator = () => {
    return (
      <View
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          height: Common.getLengthByIPhone7(200),
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator />
      </View>
    );
  };

  const renderList = () => {
    return (
      <FlatList
        style={[
          {
            width: Common.getLengthByIPhone7(0),
          },
          style,
        ]}
        contentContainerStyle={{
          alignItems: 'center',
        }}
        refreshControl={
          <RefreshControl
            refreshing={isRequestGoing}
            onRefresh={() => {
              if (onRefresh) {
                onRefresh();
              }
            }}
          />
        }
        data={cources}
        extraData={cources}
        keyExtractor={(item, index) => item.id}
        renderItem={renderItem}
      />
    );
  };

  const renderItem = ({item, index}) => {
    return (
      <View
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          height: Common.getLengthByIPhone7(96),
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: Common.getLengthByIPhone7(68),
            height: Common.getLengthByIPhone7(68),
            borderRadius: Common.getLengthByIPhone7(8),
            overflow: 'hidden',
          }}>
          <Image
            source={images[index % 6]}
            style={{
              width: Common.getLengthByIPhone7(68),
              height: Common.getLengthByIPhone7(68),
              resizeMode: 'cover',
            }}
          />
        </View>
        <View
          style={{
            marginLeft: Common.getLengthByIPhone7(35),
          }}>
          <Text
            style={[
              typography.CAPTION,
              {
                fontSize: Common.getLengthByIPhone7(14),
              },
            ]}
            allowFontScaling={false}>
            {item.name}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Image
              source={require('./../../assets/ic-tab5.png')}
              style={{
                width: Common.getLengthByIPhone7(10),
                height: Common.getLengthByIPhone7(10),
                resizeMode: 'contain',
                tintColor: colors.BORDER_COLOR,
              }}
            />
            <Text
              style={[
                typography.SUBTITLE,
                {
                  fontSize: Common.getLengthByIPhone7(12),
                  color: colors.BORDER_COLOR,
                  marginLeft: 6,
                },
              ]}
              allowFontScaling={false}>
              {item.speaker}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text
              style={[
                typography.CAPTION,
                {
                  color: colors.BLUE_COLOR,
                },
              ]}
              allowFontScaling={false}>
              ${item.price}
            </Text>
            <View
              style={{
                height: Common.getLengthByIPhone7(15),
                borderRadius: Common.getLengthByIPhone7(13),
                backgroundColor: '#FFEBF0',
                alignItems: 'center',
                justifyContent: 'center',
                paddingLeft: Common.getLengthByIPhone7(8),
                paddingRight: Common.getLengthByIPhone7(8),
                marginLeft: Common.getLengthByIPhone7(6),
              }}>
              <Text
                style={[
                  typography.SUBTITLE,
                  {
                    color: colors.RED_COLOR,
                    fontSize: Common.getLengthByIPhone7(10),
                    lineHeight: null,
                  },
                ]}
                allowFontScaling={false}>
                {item.hours} hours
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  return body;
};

const mstp = (state: RootState) => ({
  cources: state.user.cources,
  isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(CourcesTable);
