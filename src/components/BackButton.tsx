import React, {useEffect} from 'react';
import {TouchableOpacity, Image} from 'react-native';
import Common from './../utilities/Common';
import {useNavigation} from '@react-navigation/native';
import {colors} from '../styles';

const BackButton = props => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={{
        marginLeft: Common.getLengthByIPhone7(10),
        width: Common.getLengthByIPhone7(32),
        height: Common.getLengthByIPhone7(32),
        alignItems: 'center',
        justifyContent: 'center',
      }}
      onPress={() => {
        if (props.navigation) {
          props.navigation.goBack(null);
        } else {
          navigation.goBack(null);
        }
      }}>
      <Image
        source={require('./../assets/ic-arrow-back.png')}
        style={{
          resizeMode: 'contain',
          width: Common.getLengthByIPhone7(8),
          height: Common.getLengthByIPhone7(15),
          tintColor: colors.TEXT_COLOR,
        }}
      />
    </TouchableOpacity>
  );
};

export default BackButton;
