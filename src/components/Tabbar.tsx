import React, {useCallback, useEffect, useRef} from 'react';
import {
  Image,
  ImageBackground,
  View,
  TouchableOpacity,
  TextInput,
  Linking,
  Text,
  Alert,
  Dimensions,
} from 'react-native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {typography, colors, shadows} from './../styles';

const imagesAct = [
  require('./../assets/ic-tab1.png'),
  require('./../assets/ic-tab2.png'),
  require('./../assets/ic-tab3.png'),
  require('./../assets/ic-tab4.png'),
  require('./../assets/ic-tab5.png'),
];

const labels = ['Home', 'Course', 'Search', 'Message', 'Account'];

const Tabbar = ({state, descriptors, navigation}) => {
  // const navigation = useNavigation();
  const [code, setCode] = React.useState('');

  return (
    <View
      style={{
        flexDirection: 'row',
        backgroundColor: 'white',
        height: 111,
        width: Dimensions.get('screen').width,
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        if (index === 2) {
          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}>
              <View
                style={{
                  marginBottom: 37,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    width: 52,
                    height: 52,
                    borderRadius: 26,
                    backgroundColor: isFocused
                      ? colors.BLUE_COLOR
                      : 'rgba(61, 92, 255, 0.05)',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    source={imagesAct[index]}
                    style={{
                      width: 17,
                      height: 17,
                      resizeMode: 'contain',
                      tintColor: isFocused ? 'white' : colors.BLUE_COLOR,
                    }}
                  />
                </View>
                <Text
                  style={[
                    typography.SUBTITLE,
                    {
                      marginTop: 2,
                      color: isFocused
                        ? colors.BLUE_COLOR
                        : colors.BORDER_COLOR,
                      fontSize: 11,
                      lineHeight: null,
                    },
                  ]}
                  allowFontScaling={false}>
                  {labels[index]}
                </Text>
              </View>
            </TouchableOpacity>
          );
        } else {
          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}>
              <View
                style={{
                  marginBottom: 37,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    width: 26,
                    height: 2,
                    backgroundColor: isFocused ? colors.BLUE_COLOR : 'white',
                  }}
                />
                <Image
                  source={imagesAct[index]}
                  style={{
                    width: 24,
                    height: 24,
                    resizeMode: 'contain',
                    marginTop: 12,
                    tintColor: isFocused
                      ? colors.BLUE_COLOR
                      : colors.TABBAR_INACTIVE_COLOR,
                  }}
                />
                <Text
                  style={[
                    typography.SUBTITLE,
                    {
                      marginTop: 7,
                      color: isFocused
                        ? colors.BLUE_COLOR
                        : colors.BORDER_COLOR,
                      fontSize: 11,
                      lineHeight: null,
                    },
                  ]}
                  allowFontScaling={false}>
                  {labels[index]}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }
      })}
    </View>
  );
};

const mstp = (state: RootState) => ({
  // isRequestGoing: state.user.isRequestGoing,
  // userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
  // sendCode: payload => dispatch.user.sendCode(payload),
  // getProfile: () => dispatch.user.getProfile(),
});

export default connect(mstp, mdtp)(Tabbar);
