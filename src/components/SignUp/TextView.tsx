import React, {useEffect, useRef} from 'react';
import {Image, TextInput, View, Text, TouchableOpacity} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors, typography} from '../../styles';

const TextView = ({
  title,
  value,
  style,
  keyboardType,
  secureTextEntry,
  onChange,
  placeholder,
  autoFocus,
  error,
}) => {
  const [text, setText] = React.useState('');
  const [hasError, setHasError] = React.useState(false);
  const [show, setShow] = React.useState(false);

  useEffect(() => {
    setShow(secureTextEntry);
  }, []);

  useEffect(() => {
    setHasError(error);
  }, [error]);

  useEffect(() => {
    if (onChange) {
      onChange(text);
    }
  }, [text]);

  useEffect(() => {
    setText(value);
  }, [value]);

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(48),
        },
        style,
      ]}>
      <Text
        style={[
          typography.SUBTITLE,
          {
            color: colors.LIGHT_BLUE_COLOR,
            fontSize: Common.getLengthByIPhone7(14),
          },
        ]}
        numberOfLines={1}
        allowFontScaling={false}>
        {title}
      </Text>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TextInput
          style={[
            typography.SUBTITLE,
            {
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(48),
              height: Common.getLengthByIPhone7(50),
              borderRadius: Common.getLengthByIPhone7(12),
              borderColor: hasError ? colors.ERROR_COLOR : colors.BORDER_COLOR,
              borderWidth: 1,
              color: colors.TEXT_COLOR,
              fontSize: Common.getLengthByIPhone7(14),
              lineHeight: null,
              paddingLeft: Common.getLengthByIPhone7(20),
              paddingRight: Common.getLengthByIPhone7(20),
            },
          ]}
          placeholder={placeholder}
          placeholderTextColor={
            hasError ? colors.ERROR_COLOR : colors.LIGHT_BLUE_COLOR
          }
          autoFocus={autoFocus}
          secureTextEntry={show}
          returnKeyType={'done'}
          keyboardType={keyboardType}
          allowFontScaling={false}
          underlineColorAndroid={'transparent'}
          onSubmitEditing={() => {}}
          onFocus={() => {}}
          onBlur={() => {}}
          onChangeText={code => {
            setText(code);
          }}
          value={text}
        />
        {secureTextEntry ? (
          <TouchableOpacity
            style={{
              position: 'absolute',
              right: Common.getLengthByIPhone7(25),
            }}
            onPress={() => {
              setShow(!show);
            }}>
            <Image
              source={
                show
                  ? require('./../../assets/ic-eye-open.png')
                  : require('./../../assets/ic-eye-close.png')
              }
              style={{
                width: Common.getLengthByIPhone7(18),
                height: Common.getLengthByIPhone7(12),
              }}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(TextView);
