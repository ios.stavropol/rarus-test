import React, {useEffect, useRef} from 'react';
import {Image, TextInput, View, Text, TouchableOpacity} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors, typography} from '../../styles';

const Button = ({style, onClick, disabled}) => {
  const [enabled, setEnabled] = React.useState(false);

  useEffect(() => {
    setEnabled(!disabled);
  }, [disabled]);

  return (
    <TouchableOpacity
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(48),
          height: Common.getLengthByIPhone7(50),
          borderRadius: Common.getLengthByIPhone7(12),
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: colors.BLUE_COLOR,
          opacity: enabled ? 1 : 0.5,
        },
        style,
      ]}
      onPress={() => {
        if (enabled) {
          if (onClick) {
            onClick();
          }
        }
      }}>
      <Text
        style={[
          typography.CAPTION,
          {
            color: 'white',
          },
        ]}
        allowFontScaling={false}>
        Create account
      </Text>
    </TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(Button);
