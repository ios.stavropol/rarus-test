import React, {useEffect} from 'react';
import {Image, ScrollView, View, Text, TouchableOpacity} from 'react-native';
import {RootState, Dispatch} from './../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography} from './../../styles';
import Modal from 'react-native-modal';
import {FadeOut} from 'react-native-reanimated';

const ModalView = ({show, onClose}) => {
  const [body, setBody] = React.useState(null);
  const [showModal, setShowModal] = React.useState(false);

  useEffect(() => {
    setShowModal(show);
  }, [show]);

  return (
    <Modal
      isVisible={showModal}
      backdropColor={'black'}
      backdropOpacity={0.7}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          borderRadius: Common.getLengthByIPhone7(12),
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(84),
          height: Common.getLengthByIPhone7(301),
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <View
          style={{
            width: Common.getLengthByIPhone7(64),
            height: Common.getLengthByIPhone7(64),
            borderRadius: Common.getLengthByIPhone7(64) / 2,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: colors.BLUE_COLOR,
          }}>
          <Image
            source={require('./../../assets/ic-tick.png')}
            style={{
              width: Common.getLengthByIPhone7(28),
              height: Common.getLengthByIPhone7(19),
            }}
          />
        </View>
        <Text
          style={[
            typography.CAPTION,
            {
              marginTop: Common.getLengthByIPhone7(16),
            },
          ]}>
          Success
        </Text>
        <Text
          style={[
            typography.SUBTITLE,
            {
              color: colors.LIGHT_BLUE_COLOR,
              marginTop: Common.getLengthByIPhone7(12),
            },
          ]}>
          Congratulations, you have completed your registration!
        </Text>
        <TouchableOpacity
          style={{
            marginTop: Common.getLengthByIPhone7(16),
            width:
              Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(116),
            height: Common.getLengthByIPhone7(50),
            borderRadius: Common.getLengthByIPhone7(12),
            backgroundColor: colors.BLUE_COLOR,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => {
            if (onClose) {
              onClose();
            }
          }}>
          <Text
            style={[
              typography.CAPTION,
              {
                color: 'white',
              },
            ]}>
            Done
          </Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const mstp = (state: RootState) => ({
  userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
  logout: () => dispatch.user.logout(),
});

export default connect(mstp, mdtp)(ModalView);
