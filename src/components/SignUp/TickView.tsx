import React, {useEffect, useRef} from 'react';
import {Image, TextInput, View, Text, TouchableOpacity} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors, typography} from '../../styles';

const TickView = ({style, onChange}) => {
  const [selected, setSelected] = React.useState(false);

  useEffect(() => {
    if (onChange) {
      onChange(selected);
    }
  }, [selected]);

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(48),
          flexDirection: 'row',
        },
        style,
      ]}>
      <TouchableOpacity
        style={{
          width: Common.getLengthByIPhone7(16),
          height: Common.getLengthByIPhone7(16),
          borderRadius: Common.getLengthByIPhone7(3),
          marginRight: Common.getLengthByIPhone7(7),
          borderColor: colors.BORDER_COLOR,
          borderWidth: selected ? 0 : 1,
          backgroundColor: selected ? colors.BLUE_COLOR : 'transparent',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        activeOpacity={1}
        onPress={() => {
          setSelected(!selected);
        }}>
        <Image
          source={require('./../../assets/ic-tick.png')}
          style={{
            width: Common.getLengthByIPhone7(10),
            height: Common.getLengthByIPhone7(9),
            resizeMode: 'contain',
          }}
        />
      </TouchableOpacity>
      <Text
        style={[
          typography.SUBTITLE,
          {
            color: colors.GRAY_COLOR,
            fontSize: Common.getLengthByIPhone7(12),
            lineHeight: null,
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(71),
          },
        ]}
        allowFontScaling={false}>
        By creating an account you have to agree with our them & condition.
      </Text>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(TickView);
