import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import Common from '../utilities/Common';
import {useNavigation} from '@react-navigation/native';
import {useHeaderHeight} from '@react-navigation/elements';
import {colors} from '../styles';
import {typography} from '../styles';
import {isIphoneX} from 'react-native-iphone-x-helper';

const NavBar = ({title, headerLeft, headerRight, style}) => {
  const navigation = useNavigation();
  const headerHeight = useHeaderHeight();

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0),
          height: isIphoneX() ? 96 : 50,
          alignItems: 'flex-end',
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: 'white',
        },
        style,
      ]}>
      <View
        style={{
          width: Common.getLengthByIPhone7(0),
          alignItems: 'center',
          justifyContent: 'flex-start',
          flexDirection: 'row',
        }}>
        {headerLeft}
        <Text
          style={[
            typography.TITLE_32,
            {
              fontSize: Common.getLengthByIPhone7(24),
              marginLeft: Common.getLengthByIPhone7(20),
            },
            style,
          ]}>
          {title}
        </Text>
        <View
          style={{
            position: 'absolute',
            right: Common.getLengthByIPhone7(21),
          }}>
          {headerRight}
        </View>
      </View>
    </View>
  );
};

export default NavBar;
