//@ts-nocheck
import {createModel} from '@rematch/core';
import {API, StorageHelper} from './../../services';
import {ErrorsHelper} from './../../helpers';
import type {RootModel} from './../models';
import {BASE_URL} from './../../constants';
import Common from './../../utilities/Common';
import AsyncStorage from '@react-native-async-storage/async-storage';
import firestore from '@react-native-firebase/firestore';

type UserState = {
  isRequestGoing: false;
  courcesRaw: [];
  cources: [];
};

const user = createModel<RootModel>()({
  state: {
    isRequestGoing: false,
    cources: [],
    courcesRaw: [],
  } as UserState,
  reducers: {
    setRequestGoingStatus: (state, payload: boolean) => ({
      ...state,
      isRequestGoing: payload,
    }),
    setCources: (state, payload: object) => ({
      ...state,
      cources: payload,
    }),
    setCourcesRaw: (state, payload: object) => ({
      ...state,
      courcesRaw: payload,
    }),
  },
  effects: dispatch => {
    return {
      async getCources() {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          const usersCollection = firestore()
            .collection('Courses')
            .get()
            .then(querySnapshot => {
              let array = [];
              querySnapshot.forEach(documentSnapshot => {
                array.push(documentSnapshot.data());
              });
              console.warn('Total users: ', array);
              dispatch.user.setCources(array);
              dispatch.user.setCourcesRaw(array);
              dispatch.user.setRequestGoingStatus(false);
            })
            .catch(err => {
              console.warn('collection err: ', err);
              dispatch.user.setRequestGoingStatus(false);
            });
        });
      },
    };
  },
});

export default user;
