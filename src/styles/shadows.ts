import Common from "../utilities/Common";
import { colors } from ".";

export const BLUE_SHADOW = {
	shadowColor: "rgba(184, 184, 210)",
	shadowOffset: {
		width: 0,
		height: 8,
	},
	shadowOpacity: 0.20,
	shadowRadius: -4,
	elevation: 1,
};