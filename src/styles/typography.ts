import Common from "../utilities/Common";
import { colors } from ".";
// FONT FAMILY
export const FONT_FAMILY_REGULAR = 'Poppins-Regular';
export const FONT_FAMILY_BOLD = 'Poppins-Bold';
export const FONT_FAMILY_SEMIBOLD = 'Poppins-SemiBold';

export const TITLE = {
	color: colors.TEXT_COLOR,
	fontFamily: FONT_FAMILY_SEMIBOLD,
	fontWeight: '600',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(22),
	lineHeight: Common.getLengthByIPhone7(32),
};

export const TITLE_32 = {
	color: colors.TEXT_COLOR,
	fontFamily: FONT_FAMILY_SEMIBOLD,
	fontWeight: '600',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(32),
	lineHeight: Common.getLengthByIPhone7(38),
};

export const SUBTITLE = {
	color: colors.TEXT_COLOR,
	fontFamily: FONT_FAMILY_REGULAR,
	fontWeight: 'normal',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(16),
	lineHeight: Common.getLengthByIPhone7(24),
};

export const CAPTION = {
	color: colors.TEXT_COLOR,
	fontFamily: FONT_FAMILY_SEMIBOLD,
	fontWeight: '500',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(16),
	lineHeight: Common.getLengthByIPhone7(24),
};