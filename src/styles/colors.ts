export const TEXT_COLOR = '#1F1F39';
export const GRAY_COLOR = '#858597';
export const BLUE_COLOR = '#3D5CFF';
export const LIGHT_BLUE_COLOR = '#858597';
export const BORDER_COLOR = '#b8b8d2';
export const ERROR_COLOR = '#e56b6f';
export const TABBAR_INACTIVE_COLOR = '#f4f3fd';
export const RED_COLOR = '#FF6905';
export const VIOLET_COLOR = '#440687';

export const YELLOW_ACTIVE_COLOR = '#F8AA2B';
export const YELLOW_INACTIVE_COLOR = 'rgba(248, 170, 43, 0.4)';
export const BACKGROUND_COLOR = '#ffffff';
export const VIEW_COLOR = '#F8F8F8';
