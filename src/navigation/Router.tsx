import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Image, Platform, View, Text} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {colors} from '../styles';
import {typography} from '../styles';
import IntroScreen from '../screens/Intro';
import SignUpScreen from '../screens/SignUp';
import AccountScreen from '../screens/Account';
import CourseScreen from '../screens/Course';
import HomeScreen from '../screens/Home';
import SearchScreen from '../screens/Search';
import MessageScreen from '../screens/Message';
import FavoriteScreen from '../screens/Favorite';

import Tabbar from '../components/Tabbar';
import NavBar from '../components/NavBar';
import BackButton from '../components/BackButton';

export const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const HomeTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Home'}
        component={HomeScreen}
        options={{
          headerShown: true,
          gestureEnabled: false,
          headerBackTitle: '',
          headerShadowVisible: false,
          header: props => (
            <NavBar
              {...props}
              style={{
                backgroundColor: colors.BLUE_COLOR,
                color: 'white',
              }}
              title={'Hi, Kristin'}
              headerRight={
                <Image
                  source={require('./../assets/ic-avatar.png')}
                  style={{
                    width: 36,
                    height: 36,
                    resizeMode: 'contain',
                  }}
                />
              }
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

const CourseTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Course'}
        component={CourseScreen}
        options={{
          headerShown: true,
          gestureEnabled: false,
          headerBackTitle: '',
          headerShadowVisible: false,
          header: props => (
            <NavBar
              {...props}
              style={{}}
              title={'Course'}
              headerRight={
                <Image
                  source={require('./../assets/ic-avatar.png')}
                  style={{
                    width: 36,
                    height: 36,
                    resizeMode: 'contain',
                  }}
                />
              }
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

const SearchTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Search'}
        component={SearchScreen}
        options={{
          headerShown: true,
          gestureEnabled: false,
          headerBackTitle: '',
          headerShadowVisible: false,
          header: props => <NavBar {...props} title={'Search'} />,
        }}
      />
    </Stack.Navigator>
  );
};

const MessageTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Message'}
        component={MessageScreen}
        options={{
          headerShown: true,
          gestureEnabled: false,
          headerBackTitle: '',
          headerShadowVisible: false,
          header: props => <NavBar {...props} title={'Message'} />,
        }}
      />
    </Stack.Navigator>
  );
};

const AccountTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Account'}
        component={AccountScreen}
        options={{
          headerShown: true,
          gestureEnabled: false,
          headerShadowVisible: false,
          header: props => <NavBar {...props} title={'Account'} />,
        }}
      />
      <Stack.Screen
        name={'Favorite'}
        component={FavoriteScreen}
        options={{
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          header: props => (
            <NavBar
              {...props}
              title={'Favorite'}
              headerLeft={<BackButton {...props} />}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

const Tabs = () => {
  return (
    <Tab.Navigator
      tabBar={props => <Tabbar {...props} />}
      screenOptions={({route}) => ({
        lazy: false,
        tabBarStyle: {
          backgroundColor: '#faf9f8',
          borderTopWidth: 0,
          shadowColor: '#CED6E8',
          shadowOffset: {width: 0, height: 2},
          shadowOpacity: 1,
          shadowRadius: 10,
          elevation: 4,
        },
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          let title = '';
          if (route.name === 'HomeTab') {
            iconName = require('./../assets/ic-tab1.png');
            title = 'Главная';
          } else if (route.name === 'CourseTab') {
            iconName = require('./../assets/ic-tab2.png');
            title = 'Поиск';
          } else if (route.name === 'SearchTab') {
            iconName = require('./../assets/ic-tab3.png');
            title = 'Лента';
          } else if (route.name === 'MessageTab') {
            iconName = require('./../assets/ic-tab4.png');
            title = 'База знаний';
          } else if (route.name === 'AccountTab') {
            iconName = require('./../assets/ic-tab5.png');
            title = 'Меню';
          }

          return (
            <View
              style={{
                alignItems: 'center',
                flex: 1,
              }}>
              <Image
                source={iconName}
                style={{
                  marginTop: 8,
                  resizeMode: 'contain',
                  width: 24,
                  height: 24,
                  tintColor: color,
                }}
              />
              <Text
                style={{
                  marginTop: 3,
                  color: color,
                  // fontFamily: 'OpenSans-SemiBold',
                  fontWeight: '600',
                  textAlign: 'center',
                  fontSize: 10,
                }}>
                {title}
              </Text>
            </View>
          );
        },
        tabBarActiveTintColor: colors.YELLOW_ACTIVE_COLOR,
        tabBarInactiveTintColor: colors.GRAY_COLOR,
      })}>
      <Tab.Screen
        name={'HomeTab'}
        component={HomeTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
      <Tab.Screen
        name={'CourseTab'}
        component={CourseTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
      <Tab.Screen
        name={'SearchTab'}
        component={SearchTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
      <Tab.Screen
        name={'MessageTab'}
        component={MessageTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
      <Tab.Screen
        name={'AccountTab'}
        component={AccountTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};

export default Router = () => {
  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <Stack.Navigator>
          <Stack.Screen
            name={'Intro'}
            component={IntroScreen}
            options={{
              headerShown: false,
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name={'SignUp'}
            component={SignUpScreen}
            options={{
              headerShown: false,
              gestureEnabled: false,
              animation: 'slide_from_bottom',
              presentation: 'card',
            }}
          />
          <Stack.Screen
            name={'LoginIn'}
            component={Tabs}
            options={{
              animation: 'slide_from_bottom',
              presentation: 'card',
              headerShown: false,
              gestureEnabled: false,
            }}
          />
        </Stack.Navigator>
      </SafeAreaProvider>
    </NavigationContainer>
  );
};
